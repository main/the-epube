<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'Composer\\InstalledVersions' => $vendorDir . '/composer/InstalledVersions.php',
    'IdiormMethodMissingException' => $vendorDir . '/j4mie/idiorm/idiorm.php',
    'IdiormResultSet' => $vendorDir . '/j4mie/idiorm/idiorm.php',
    'IdiormString' => $vendorDir . '/j4mie/idiorm/idiorm.php',
    'IdiormStringException' => $vendorDir . '/j4mie/idiorm/idiorm.php',
    'Jumbojett\\OpenIDConnectClient' => $vendorDir . '/jumbojett/openid-connect-php/src/OpenIDConnectClient.php',
    'Jumbojett\\OpenIDConnectClientException' => $vendorDir . '/jumbojett/openid-connect-php/src/OpenIDConnectClient.php',
    'ORM' => $vendorDir . '/j4mie/idiorm/idiorm.php',
);
