'use strict';

/* global localforage, EpubeApp, App, Cookie, $ */

const DEFAULT_FONT_SIZE = 16;
const DEFAULT_FONT_FAMILY = "Arial";
const DEFAULT_LINE_HEIGHT = 140;
const MIN_LENGTH_TO_JUSTIFY = 32; /* characters */

const LOCATION_DIVISOR = 10;
const PAGE_RESET_PROGRESS = -1;

const CACHE_PREFIX = 'epube';
const CACHE_NAME = CACHE_PREFIX + '-v3';

Promise.allSettled = Promise.allSettled || ((promises) => Promise.all(
	promises.map((p) => p
		.then((value) => ({
			status: "fulfilled",
			value
		}))
		.catch((reason) => ({
			status: "rejected",
			reason
		}))
	)
));

const Reader = {
	csrf_token: "",
	last_stored_cfi: "",
	startup_finished: false,
	hyphenate: {},
	init: function() {
		this.csrf_token = Cookie.get('epube_csrf_token');

		console.log('setting prefilter for token', this.csrf_token);

		$.ajaxPrefilter(function(options, originalOptions/*, jqXHR*/) {

			if (originalOptions.type !== 'post' || options.type !== 'post') {
				return;
			}

			const datatype = typeof originalOptions.data;

			if (datatype == 'object')
				options.data = $.param($.extend(originalOptions.data, {"csrf_token": Reader.csrf_token}));
			else if (datatype == 'string')
				options.data = originalOptions.data + "&csrf_token=" + encodeURIComponent(Reader.srf_token);

			console.log('>>>', options);
		});

		$(document).on("keyup", function(e) {
			Reader.hotkeyHandler(e);
		});

		$("#left").on("mouseup", function() {
			Reader.Page.prev();
		});

		$("#right").on("mouseup", function() {
			Reader.Page.next();
		});

		Reader.Loader.init();
	},
	onOfflineModeChanged: function(offline) {
		console.log('onOfflineModeChanged', offline);

		if (!offline && window.book) {
			const book = window.book;

			console.log("we're online, storing lastread");

			const currentCfi = book.rendition.currentLocation().start.cfi;
			const currentPage = parseInt(book.locations.percentageFromCfi(currentCfi) * 100);

			$.post("backend.php", { op: "storelastread", id: $.urlParam("id"), page: currentPage,
				cfi: currentCfi, timestamp: new Date().getTime() }, function() {
					//
				})
				.fail(function(e) {
					if (e && e.status == 401) {
						window.location = "index.php";
					}
				});
		}
	},
	initSecondStage: async function() {

		if (typeof EpubeApp != "undefined") {
			EpubeApp.setPage("PAGE_READER");
		} else {
			$(window).on('online', function() {
				Reader.onOfflineModeChanged(false);
			});

			$(window).on('offline', function() {
				Reader.onOfflineModeChanged(true);
			});
		}

		Reader.applyTheme();

		const bookinfo = await localforage.getItem(Reader.cacheId());

		if (!bookinfo) {
			const bookId = $.urlParam("b");

			console.log('requesting bookinfo for book', bookId)

			try {
				const resp = await $.post("backend.php", {op: "getinfo", id: bookId });

				if (resp.has_cover) {
					const cover = await fetch("backend.php?op=cover&id=" + bookId);

					if (cover.ok)
						await localforage.setItem(Reader.cacheId('cover'), cover.blob());
				}

				await localforage.setItem(Reader.cacheId(), resp);

				console.log('bookinfo saved');

			} catch (e) {
				$(".loading-message").html(`Unable to load book info.<br/><small>${e.responseText}</small>`);
				return;
			}
		}

		console.log('trying to load book...');

		let book_data = await localforage.getItem(Reader.cacheId("book"));

		if (!book_data) {
			console.log("local data not found, loading from network...");

			$(".loading-message").html("Downloading book…");

			const book_resp = await fetch("backend.php?op=download&id=" + $.urlParam("id"));

			if (book_resp.ok) {
				book_data = await book_resp.blob();

				if (book_data) {
					await localforage.setItem(Reader.cacheId('book'), book_data);
					console.log('saved local data for book');
				} else {
					$(".loading-message").html("Unable to get book blob data.");
					return;
				}

			} else {
				$(".loading-message").html(`Unable to download book: ${book_resp.status}`);
				return;
			}
		}

		const fileReader = new FileReader();

		fileReader.onload = function() {
			try {
				Reader.openBookData(this.result);
			} catch (e) {
				console.error(e);
				$(".loading-message").html("Unable to load book blob: " + e);
			}
		};

		fileReader.onerror = (e) => {
			console.log('filereader error', e);
			$(".loading-message").html("Unable to open book.<br/><small>" + e + "</small>");
		};

		fileReader.readAsArrayBuffer(book_data);
	},
	openBookData: async function(book_data) {
		/* global ePub */
		const book = ePub();
		window.book = book;

		try {
			await book.open(book_data);
		} catch (e) {
			console.error(e);
			$(".loading-message").html("Unable to open book.<br/><small>" + e.message + "</small>");
			return;
		}

		await book.ready;

		console.log('book is ready');

		await Reader.showBookInfo(book);
		await Reader.loadLocations(book);

		const rendition = book.renderTo("reader", {
			width: '100%',
			height: '100%',
			allowScriptedContent: true,
			minSpreadWidth: 961
		});

		$(".location").click(async function() {
			const current = Math.floor(rendition.currentLocation().start.location / LOCATION_DIVISOR);
			const total = Math.floor(book.locations.length() / LOCATION_DIVISOR);

			const page = prompt("Jump to location [1-" + total + "]", current);

			if (page)
				await rendition.display(book.locations._locations[page * LOCATION_DIVISOR]);
		});

		const enable_hyphens = await localforage.getItem("epube.enable-hyphens");

		if (enable_hyphens) {
			/* global hyphenationPatternsEnUs, hyphenationPatternsRu, createHyphenator */
			Reader.hyphenate.en = createHyphenator(hyphenationPatternsEnUs, { html: true });
			Reader.hyphenate.ru = createHyphenator(hyphenationPatternsRu, { html: true });
		}

		Reader.applyStyles(true);

		console.log('book displayed');

		Reader.initRenditionHooks(book, rendition);
		Reader.initModals();
		Reader.initToc(book);
		Reader.initStyleHooks(book);
		await Reader.Page.openLastRead();

		console.log('startup finished');
		Reader.startup_finished = true;
	},
	showBookInfo: async function(book) {
		const bookinfo = await localforage.getItem(Reader.cacheId());

		let title;
		let author;

		if (bookinfo) {
			title = bookinfo.title;
			author = bookinfo.author_sort;
		} else {
			const metadata = book.package.metadata;

			title = metadata.title;
			author = metadata.creator;
		}

		document.title = title + " – " + author + " – The Epube";

		$(".title")
			.text(title)
			.attr("title", title + " – " + author);

		if (typeof EpubeApp != "undefined") {
			EpubeApp.setTitle(title);
			EpubeApp.showActionBar(false);
		}
	},
	loadLocations: async function(book) {
		let locations = await localforage.getItem(Reader.cacheId("locations"));

		// legacy format is array of objects {cfi: ..., page: ...}
		if (locations && typeof locations[0] == "string") {
			console.log('loading local locations...');
			try {
				await book.locations.load(locations);
				return;
			} catch (e) {
				console.warn(e);
			}
		}

		if (App.isOnline()) {
			console.log("downloading locations...");

			$(".loading-message").html("Downloading locations…");

			const resp = await fetch("backend.php?op=getpagination&id=" + $.urlParam("id"));

			if (resp.ok) {
				console.log("got locations from server");

				locations = await resp.json();

				if (locations && typeof locations[0] == "string") {
					console.log('saving locations locally...');

					await localforage.setItem(Reader.cacheId("locations"), locations);

					try {
						book.locations.load(locations);

						return;
					} catch (e) {
						console.warn(e);
					}
				}
			}
		}

		$(".loading-message").html("Preparing locations…");

		locations = await book.locations.generate(100);

		$.post("backend.php", { op: "storepagination", id: $.urlParam("id"),
			payload: JSON.stringify(locations), total: 100});

		await localforage.setItem(Reader.cacheId("locations"), locations);
	},
	initRenditionHooks: function(book, rendition) {
		rendition.hooks.content.register(async (contents) => {

			contents.on("linkClicked", (href) => {
				console.log('linkClicked', href);

				Reader.Page.blockLastReadUpdate();

				if (href.indexOf("://") == -1) {
					$(".prev_location_btn")
						.attr("data-location-cfi", rendition.currentLocation().start.cfi)
						.show();

					window.setTimeout(function() {
						Reader.showUI(true);
					}, 50);
				}
			});

			// get our stuff from resource loader and inject into iframe content

			const base_url = window.location.href.match(/^.*\//)[0];

			if (typeof EpubeApp != "undefined") {
				$(contents.document.head).append($(`<link type="text/css" rel="stylesheet" media="screen" href="/assets/android.css" />`));
			} else {
				$(contents.document.head)
					.append($("<style type='text/css'>")
						.text(Reader.generateFontsCss(base_url)));
			}

			const script_names = [
				"dist/app-libs.min.js",
				"dist/reader_iframe.min.js"
			];

			for (let i = 0; i < script_names.length; i++) {

				// we need to create script element with proper context, that is inside the iframe
				const elem = contents.document.createElement("script");
				elem.type = 'text/javascript';
				elem.text = Reader.Loader._res_data[base_url + script_names[i]];

				contents.document.head.appendChild(elem);
			}

			$(contents.document.head)
				.append($("<style type='text/css'>")
					.text(Reader.Loader._res_data[base_url + 'dist/reader_iframe.min.css']));

			const theme = await localforage.getItem("epube.theme") || 'default';

			$(contents.document).find("body")
				.attr("class", typeof EpubeApp != "undefined" ? "is-epube-app" : "")
				.addClass("theme-" + theme);

		});

		rendition.on("keyup", (e) => {
			Reader.hotkeyHandler(e);
		});

		rendition.on('rendered', function(/*chapter*/) {
			$(".chapter").html($("<span>").addClass("glyphicon glyphicon-th-list"));

			// TODO: this needs a proper implementation instead of a timeout hack
			setTimeout(() => {
				Reader.Page._moved_next = 0;
			}, 150);

			Reader.applyTheme();
			Reader.resizeSideColumns();

			try {
				const location = rendition.currentLocation();

				if (location.start) {
					const cur_href = book.canonical(location.start.href);
					let toc_entry = false;

					$.each(Reader.flattenToc(book), function(i, r) {

						if (book.spine.get(r.href).canonical == cur_href) {
							toc_entry = r;
							return;
						}
					});

					if (toc_entry && toc_entry.label)
						$(".chapter").append("&nbsp;" + toc_entry.label.trim() + " | ");

					Reader.generateTocBar(book, Reader.flattenToc(book));
				}

			} catch (e) {
				console.warn(e);
			}
		});

		rendition.on('relocated', (location) => Reader.Page.onRelocated(book, location));
	},
	initModals: function() {
		$('#settings-modal').on('shown.bs.modal', async function() {

			const last_read = await localforage.getItem(Reader.cacheId("lastread"));

			if (last_read && last_read.cfi) {
				$(".lastread_input")
					.val(last_read.page + '%')
					.attr('title', `[local] ${last_read.cfi}`);
			}

			if (navigator.onLine)
				$.post("backend.php", { op: "getlastread", id: $.urlParam("id") }, (data) => {
					$(".lastread_input")
						.val(data.page + '%')
						.attr('title', `[remote] ${data.cfi}`);
				});

			$(".enable_hyphens_checkbox")
				.attr("checked", await localforage.getItem("epube.enable-hyphens"))
				.off("click")
				.on("click", function(evt) {
					localforage.setItem("epube.enable-hyphens", evt.target.checked);

					if (confirm("Toggling hyphens requires page reload. Reload now?")) {
						window.location.reload();
					}
				});

			if (typeof EpubeApp != "undefined") {
				$(".keep_ui_checkbox").parent().parent().hide();
			} else {
				$(".keep_ui_checkbox")
				.attr("checked", await localforage.getItem("epube.keep-ui-visible"))
				.off("click")
				.on("click", function(evt) {
					localforage.setItem("epube.keep-ui-visible", evt.target.checked);
				});
			}

			const enable_hacks = await localforage.getItem("epube.enable-column-hacks");

			$(".enable_column_hacks_checkbox")
				.attr("checked", enable_hacks)
				.off("click")
				.on("click", function(evt) {
					localforage.setItem("epube.enable-column-hacks", evt.target.checked);
				});

			const stamp = await localforage.getItem("epube.cache-timestamp");
			const version = await localforage.getItem("epube.cache-version");

			$(".last-mod-timestamp").html(`${version}
				&mdash; ${parseInt(stamp) ? new Date(stamp*1000).toLocaleString("en-GB") : "Unknown"}
				(${App.isOnline() ? `<span class='text-success'>Online</span>` : `<span class='text-danger'>Offline</span>`})
				`);

			$(".font_family").val(await localforage.getItem("epube.fontFamily") || DEFAULT_FONT_FAMILY);
			$(".theme_name").val(await localforage.getItem("epube.theme") || 'default');

			const zoom = $(".font_size").html("");

			for (let i = 10; i <= 32; i++) {
				const opt = $("<option>").val(i).html(i + " px");
				zoom.append(opt);
			}

			zoom.val(await localforage.getItem("epube.fontSize") || DEFAULT_FONT_SIZE);

			const line_height = $(".line_height").html("");

			for (let i = 100; i <= 220; i += 10) {
				const opt = $("<option>").val(i).html(i + "%");
				line_height.append(opt);
			}

			line_height.val(await localforage.getItem("epube.lineHeight") || DEFAULT_LINE_HEIGHT);

		});

		$('#dict-modal').on('shown.bs.modal', function() {
			$(".dict_result").scrollTop(0);
		});

		// TODO: make configurable
		$(".dict_search_btn").on("click", function() {
			$("#dict-modal").modal('hide');
			window.open("https://duckduckgo.com/?q=" + $(".dict_query").val());
		});

		$(".wiki_search_btn").on("click", function() {
			$(".dict_result").html("Loading, please wait...");

			$.post("backend.php", {op: "wikisearch", query: $(".dict_query").val()})
				.then((resp) => {
					try {
						console.log('wikisearch resp', resp);

						let tmp = "";

						$.each(resp.query.pages, (i,p) => {
							tmp += p.extract;
						});

						$(".dict_result").html(tmp && tmp != "undefined" ? tmp : "No definition found for " + $(".dict_query").val() + ".");
					} catch (e) {
						console.error(e);
						$(".dict_result").text("Error while processing data: " + e);
					}
				})
				.fail((e) => {
					console.error(e);
					$(".dict_result").text("Error while retrieving data.");
				})
		});
	},
	initToc: function(book) {
		function toc_loc_msg(href) {
			try {
				const cfiBase = book.spine.get(href).cfiBase;

				const loc = book.locations._locations.find(function(k) {
					return k.indexOf(cfiBase) != -1
				});

				return parseInt(window.book.locations.locationFromCfi(loc) / LOCATION_DIVISOR);

			} catch (e) {
				console.warn(e);
			}

			return "";
		}

		function process_toc_sublist(row, list, nest) {

			if (nest == 3) return false;

			if (row.subitems) {

				const sublist = $("<ul class='toc_sublist list-unstyled'>");

				$.each(row.subitems, function(i, row) {

					const a = $("<a>")
						.attr('href', '#')
						.html("<b class='pull-right'>" + toc_loc_msg(row.href) + "</b>" + row.label)
						.attr('data-href', row.href)
						.click(function() {
							book.rendition.display(a.attr('data-href'));
						});

					sublist.append($("<li>").append(a));

					process_toc_sublist(row, sublist, nest + 1);

				});

				list.append(sublist);
			}
		}

		$('#toc-modal').on('shown.bs.modal', function() {

			const toc = book.navigation.toc;

			const list = $(".toc_list");
			list.html("");

			$.each(toc, function(i, row) {

				// if anything fails here the toc entry is likely useless anyway (i.e. no cfi)
				try {
					const a = $("<a>")
						.attr('href', '#')
						.html("<b class='pull-right'>" + toc_loc_msg(row.href) + "</b>" + row.label)
						.attr('data-href', row.href)
						.click(function() {
							book.rendition.display(a.attr('data-href'));
						});

					list.append($("<li>").append(a));

					process_toc_sublist(row, list, 0);

				} catch (e) {
					console.warn(e);
				}
			});

			// well the toc didn't work out, might as well generate one
			if (list.children().length <= 1) {

				list.html("");

				$.each(book.spine.items, function (i, row) {

					const a = $("<a>")
						.attr('href', '#')
						.attr('title', row.url)
						.html("Section " + (i+1))
						.attr('data-href', row.href)
						.click(function() {
							book.rendition.display(a.attr('data-href'));
						});

					list.append($("<li>").append(a));

				});
			}

		});
	},
	initStyleHooks: function(book) {
		/* embedded styles may conflict with our font sizes, etc */
		book.spine.hooks.content.register(function(doc/*, section */) {

			$(doc).find("p")
					.filter((i, e) => (($(e).text().length >= MIN_LENGTH_TO_JUSTIFY) ? e : null))
						.css("text-align", "justify");

			$(doc).find("html")
					.attr("class", "");

			$(doc).find("pre")
					.css("white-space", "pre-wrap");

			$(doc).find("a, p, span, em, i, strong, b, body, header, section, div, big, small, table, tr, td, ul, ol, li")
					.attr("class", "")
					.css("font-family", "inherit")
					.css("font-size", "inherit")
					.css("line-height", "inherit")
					.css("color", "")
					.css("border", "none ! important")
					.css("background", "")
					.css("background-color", "")
					.attr("width", "")
					.attr("height", "");

			// same as above except for allowed font-size
			$(doc).find("h1, h2, h3, h4, h5")
				.attr("class", "")
				.css("font-family", "inherit")
				.css("color", "")
				.css("border", "none ! important")
				.css("background", "")
				.css("background-color", "");

			if (typeof Reader.hyphenate.en != "undefined") {
				$(doc).find('p, div').each((i,p) => {
					p = $(p);

					p.html(Reader.hyphenate.en(p.html()));
					p.html(Reader.hyphenate.ru(p.html()));
				});
			}
		});
	},
	generateFontsCss: function(base_url) {
		return `
			/** Calibri */

			@font-face {
				font-family: 'Calibri';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/calibriz.ttf']});
				font-style: italic;
				font-weight : 700;
			}

			@font-face {
				font-family: 'Calibri';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/calibrib.ttf']});
				font-style: normal;
				font-weight : 700;
			}

			@font-face {
				font-family: 'Calibri';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/calibrii.ttf']});
				font-style: italic;
				font-weight : 400;
			}

			@font-face {
				font-family: 'Calibri';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/calibri.ttf']});
				font-style: normal;
				font-weight : 400;
			}

			/** Cambria */

			@font-face {
				font-family: 'Cambria';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/cambriab.ttf']});
				font-style: normal;
				font-weight : 700;
			}

			@font-face {
				font-family: 'Cambria';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/cambriaz.ttf']});
				font-style: italic;
				font-weight : 700;
			}

			@font-face {
				font-family: 'Cambria';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/cambriai.ttf']});
				font-style: italic;
				font-weight : 400;
			}

			@font-face {
				font-family: 'Cambria';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/cambria.ttf']});
				font-style: normal;
				font-weight : 400;
			}

			/** Caecilia */

			@font-face {
				font-family: Caecilia;
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/caecilia.ttf']});
				font-weight : normal;
			}

			@font-face {
				font-family: Caecilia;
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/caeciliab.ttf']});
				font-weight : bold;
			}

			@font-face {
				font-family: Caecilia;
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/caeciliai.ttf']});
				font-style : italic;
			}

			/** Palatino */

			@font-face {
				font-family: 'Palatino';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/palab.ttf']});
				font-style: normal;
				font-weight : 700;
			}

			@font-face {
				font-family: 'Palatino';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/palabi.ttf']});
				font-style: italic;
				font-weight : 700;
			}

			@font-face {
				font-family: 'Palatino';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/palai.ttf']});
				font-style: italic;
				font-weight : 400;
			}

			@font-face {
				font-family: 'Palatino';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/pala.ttf']});
				font-style: normal;
				font-weight : 400;
			}

			/** Consolas */

			@font-face {
				font-family: 'Consolas';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/consolab.ttf']});
				font-style: normal;
				font-weight : 700;
			}

			@font-face {
				font-family: 'Consolas';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/consolaz.ttf']});
				font-style: italic;
				font-weight : 700;
			}

			@font-face {
				font-family: 'Consolas';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/consolai.ttf']});
				font-style: italic;
				font-weight : 400;
			}

			@font-face {
				font-family: 'Consolas';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/consola.ttf']});
				font-style: normal;
				font-weight : 400;
			}

			/** IBM Plex Mono */

			@font-face {
				font-family: 'IBM Plex Mono';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/IBMPlexMono-Bold.otf']});
				font-style: normal;
				font-weight : 700;
			}

			@font-face {
				font-family: 'IBM Plex Mono';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/IBMPlexMono-BoldItalic.otf']});
				font-style: italic;
				font-weight : 700;
			}

			@font-face {
				font-family: 'IBM Plex Mono';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/IBMPlexMono-TextItalic.otf']});
				font-style: italic;
				font-weight : 400;
			}

			@font-face {
				font-family: 'IBM Plex Mono';
				src: url(${Reader.Loader._res_data[base_url + 'lib/fonts/IBMPlexMono-Text.otf']});
				font-style: normal;
				font-weight : 400;
			}
			`;
	},
	flattenTocSubItems: function(entry, nest) {
		let rv = [];

		if (nest == 3) return false;

		if (entry.subitems) {
			$.each(entry.subitems, function (i, r) {
				r._nest = nest;

				rv.push(r);
				rv = rv.concat(Reader.flattenTocSubItems(r, nest+1));
			});
		}

		return rv;
	},
	flattenToc: function(book) {
		if (this._flattened_toc) {
			return this._flattened_toc;
		} else {
			let rv = [];

			$.each(book.navigation.toc, function(i, r) {
				r._nest = 0;

				rv.push(r);
				rv = rv.concat(Reader.flattenTocSubItems(r, 1));
			});

			this._flattened_toc = rv;

			return rv;
		}
	},
	generateTocBar: function(book, toc) {
		$(".spacer")
			.html("");

		$.each(toc, function(i, te) {
			try {
				const cfiBase = book.spine.get(te.href).cfiBase;
				const loc = book.locations._locations.find(function(k) {
					return k.indexOf(cfiBase) != -1
				});

				if (loc) {
					const pct = Math.round(book.locations.percentageFromCfi(loc) * 100);

					$(".spacer").append(
						$("<div class='toc-bar-entry'>")
							.attr('data-nest-level', te._nest)
							.css('left', pct + '%')
							.css('_width', (3 - te._nest) + "px")
							.attr("title", te.label)
						)

				}

			} catch (e) {
				console.warn(e);
			}
		});

		$(".spacer").append($("<div class='toc-bar-entry current-position'>"));

		Reader.updateTocBarPosition(book, book.rendition.currentLocation())
	},
	updateTocBarPosition: function(book, location) {
		const cur_pct = Math.round(location.start.location / book.locations.length() * 100);

		$(".toc-bar-entry.current-position")
				.css('left', cur_pct + '%');
	},
	applyStyles: function(default_only) {
		Promise.all([
			localforage.getItem("epube.fontSize"),
			localforage.getItem("epube.fontFamily"),
			localforage.getItem("epube.lineHeight"),
			localforage.getItem("epube.theme")
		]).then(function(res) {
			const fontSize = res[0] ? res[0] + "px" : DEFAULT_FONT_SIZE + "px";
			const fontFamily = res[1] ? res[1] : DEFAULT_FONT_FAMILY;
			const lineHeight = res[2] ? res[2] + "%" : DEFAULT_LINE_HEIGHT + "%";
			//const themeName = res[3] ? res[3] : false;

			console.log('style', fontFamily, fontSize, lineHeight);

			console.log('applying default theme...');

			window.book.rendition.themes.default({
				html: {
					'font-size': fontSize,
					'font-family': "'" + fontFamily + "'",
					'line-height': lineHeight,
					'text-align': 'justify',
					'text-indent': '1em'
				}
			});

			if (!default_only) {
				console.log('applying rendition themes...');

				$.each(window.book.rendition.getContents(), function(i, c) {
					c.css("font-size", fontSize);
					c.css("font-family", "'" + fontFamily + "'");
					c.css("line-height", lineHeight);
					c.css("text-align", 'justify');
				});
			}

			Reader.applyTheme();
		});

	},
	applyTheme: async function() {
		let theme = await localforage.getItem("epube.theme") || "default";

		console.log('called for theme', theme);

		if (theme == "default" && typeof EpubeApp != "undefined")
			if (EpubeApp.isNightMode())
				theme = "night";

		console.log('setting main UI theme', theme);

		$("body")
			.attr("class", typeof EpubeApp != "undefined" ? "is-epube-app" : "")
			.addClass("epube-reader theme-" + theme)
			.attr("data-is-loading", "false");

		if (typeof EpubeApp != "undefined") {
			window.setTimeout(function() {
				const bg_color = window.getComputedStyle(document.querySelector("body"), null)
					.getPropertyValue("background-color");

				const match = bg_color.match(/rgb\((\d{1,}), (\d{1,}), (\d{1,})\)/);

				if (match) {
					console.log("sending bgcolor", match);

					EpubeApp.setStatusBarColor(parseInt(match[1]), parseInt(match[2]), parseInt(match[3]));
				}
			}, 250);
		}

		if (window.book)
			$.each(window.book.rendition.getContents(), function(i, c) {
				console.log('applying rendition theme', theme, 'to', c, c.document);

				$(c.document).find("body")
					.attr("class", typeof EpubeApp != "undefined" ? "is-epube-app" : "")
					.addClass("theme-" + theme);
			});
	},
	hotkeyHandler: function(e) {
		try {
			//console.log('K3:' + e.which, e);

			if ($(".modal").is(":visible"))
				return;

			// right or space or pagedown
			if (e.which == 39 || e.which == 32 || e.which == 34) {
				e.preventDefault();
				Reader.Page.next();
			}

			// left or pageup
			if (e.which == 37 || e.which == 33) {
				e.preventDefault();
				Reader.Page.prev();
			}

			// esc
			if (e.which == 27) {
				e.preventDefault();
				Reader.showUI(true);
			}
		} catch (e) {
			console.warn(e);
		}
	},
	resizeSideColumns: function() {
		let width = $("#reader").position().left;
		const iframe = $("#reader iframe")[0];

		if (iframe && iframe.contentWindow.$)
			width += parseInt(iframe.contentWindow.$("body").css("padding-left"));

		$("#left, #right").width(width);
	},
	markAsRead: async function() {
		if (confirm("Mark book as read?")) {
			const total = 100;
			const lastCfi = window.book.locations.cfiFromPercentage(1);
			const lastread_timestamp = new Date().getTime();

			if (App.isOnline()) {
				const data = await $.post("backend.php", { op: "storelastread", page: total, cfi: lastCfi, id: $.urlParam("id"), timestamp: lastread_timestamp });
				$(".lastread_input").val(data.page + '%');
			}

			await localforage.setItem(Reader.cacheId("lastread"),
				{cfi: lastCfi, page: total, total: total, timestamp: lastread_timestamp});
		}
	},
	close: async function() {
		const location = window.book.rendition.currentLocation();

		const currentCfi = location.start.cfi;
		const currentPage = parseInt(window.book.locations.percentageFromCfi(currentCfi) * 100);
		const totalPages = 100;
		const lastread_timestamp = new Date().getTime();

		await localforage.setItem(Reader.cacheId("lastread"),
			{cfi: currentCfi, page: currentPage, total: totalPages, timestamp: lastread_timestamp});

		if (App.isOnline()) {
			$.post("backend.php", { op: "storelastread", id: $.urlParam("id"), page: currentPage,
				cfi: currentCfi, timestamp: lastread_timestamp }, function() {
				window.location = $.urlParam("rt") ? "index.php?mode=" + $.urlParam("rt") : "index.php";
			})
				.fail(function() {
					window.location = "index.php";
				});
		} else {
			window.location = "index.php";
		}
	},
	cacheId: function(suffix) {
		return "epube-book." + $.urlParam("b") + (suffix ? "." + suffix : "");
	},
	toggleFullscreen: function() {
		if (typeof EpubeApp != "undefined") {
			/* noop, handled elsewhere */
		} else {
			const element = document.documentElement;
			const isFullscreen = document.webkitIsFullScreen || document.mozFullScreen || false;

			element.requestFullScreen = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen ||
				function () { return false; };

			document.cancelFullScreen = document.cancelFullScreen || document.webkitCancelFullScreen || document.mozCancelFullScreen ||
				function () { return false; };

			isFullscreen ? document.cancelFullScreen() : element.requestFullScreen();
		}
	},
	showUI: function(show) {
		if (show)
			$(".header,.footer").fadeIn();
		else
			$(".header,.footer").fadeOut();
	},
	toggleUI: function() {
		if ($(".header").is(":visible"))
			$(".header,.footer").fadeOut();
		else
			$(".header,.footer").fadeIn();
	},
	lookupWord: async function(word, callback) {
		word = word.replace(/­/g, "");

		$(".dict_result").html('Loading, please wait...');

		$("#dict-modal").modal('show');

		try {
			const data = await $.post("backend.php", {op: 'define', word: word});

			if (data) {

				$(".dict_result").html(data.result.join("<br/>"));
				$(".dict_query").val(word);

				if (callback) callback();
			}
		} catch (e) {
			console.error(e);

			$(".dict_result").html(`Network error while looking up word: ${e.responseText}.`);
		}
	},
	search: function() {
		const query = $(".search_input").val();
		const list = $(".search_results");

		list.html("");

		if (query) {

			/* eslint-disable prefer-spread */
			Promise.all(
				window.book.spine.spineItems.map(
					(item) => item.load(window.book.load.bind(window.book))
						.then(item.find.bind(item, query))
						.finally(item.unload.bind(item)))
			)
				.then((results) => Promise.resolve([].concat.apply([], results)))
				.then(function(results) {
					$.each(results, function (i, row) {
						const a = $("<a>")
							.attr('href', '#')
							.html("<b class='pull-right'>" + window.book.locations.locationFromCfi(row.cfi) + "</b>" + row.excerpt)
							.attr('data-cfi', row.cfi)
							.attr('data-id', row.id)
							.click(function() {
								window.book.rendition.display(a.attr('data-cfi'));
							});

						list.append($("<li>").append(a));
					});
				});
		}
	},
	Loader: {
		_res_data: [],
		init: async function() {

			// we need to manually provide resources for reader iframe because service worker fetch() handler doesn't work inside iframes (chrome bug?)

			const promises = [];
			let load_failed = false;

			const cache = await caches.open(CACHE_NAME);
			const cache_keys = await cache.keys();

			cache_keys.forEach((req) => {
				if (!req.url.match('backend\.php')) {
					console.log('[loader] loading resource', req.url);
					const promise = fetch(req.url).then(async (resp) => {
						if (resp.ok) {
							const url = new URL(resp.url);
							url.searchParams.delete("ts");

							if (resp.url.match('\\.ttf|\\.otf')) {
								const blob = await resp.blob();
								console.log('[loader] storing blob', url.href);

								Reader.Loader._res_data[url.toString()] = URL.createObjectURL(blob);
							} else {
								const text = await resp.text();
								console.log('[loader] storing text', url.href);

								Reader.Loader._res_data[url.toString()] = text;
							}
						} else {
							console.warn('[loader] failed for resource', req.url, resp);

							$(".loading-message").html(`Unable to load resource.<br/><small>${req.url}</small>`);

							load_failed = true;
						}
					});

					promises.push(promise);
				}
			});

			await Promise.allSettled(promises);

			console.log('[loader] resource load complete, failed: ', load_failed);

			if (!load_failed)
				Reader.initSecondStage();
			else
				Reader.applyTheme(); // fade in, etc

		},
	},
	Page: {
		_moved_next: 0,
		_prevent_lastread_timeout: false,
		_prevent_lastread: false,
		next: function() {
			window.book.rendition.next();

			Reader.Page._moved_next += 1;

			if (typeof EpubeApp != "undefined")
				EpubeApp.showActionBar(false);
			else
				localforage.getItem("epube.keep-ui-visible").then(function(keep) {
					if (!keep) Reader.showUI(false);
				});
		},
		prev: function() {
			window.book.rendition.prev();

			if (typeof EpubeApp != "undefined")
				EpubeApp.showActionBar(false);
			else
				localforage.getItem("epube.keep-ui-visible").then(function(keep) {
					if (!keep) Reader.showUI(false);
				});
		},
		openPrevious: async function(elem) {
			const cfi = $(elem).attr("data-location-cfi");

			if (cfi)
				await window.book.rendition.display(cfi);

			$(elem).fadeOut();
		},
		blockLastReadUpdate: function() {
			console.log('blocking last read updates...');

			if (Reader.Page._prevent_lastread_timeout)
				window.clearTimeout(Reader.Page._prevent_lastread_timeout);

			Reader.Page._prevent_lastread_timeout = window.setTimeout(() => {
				Reader.Page._prevent_lastread = false;
				Reader.Page._prevent_lastread_timeout = false;
				console.log('unblocked last read updates');
			}, 1000);

			Reader.Page._prevent_lastread = true;
		},
		onRelocated: async function (book, location) {
			const rendition = book.rendition;

			// locations not generated yet
			if (book.locations.length() == 0)
				return;

			const column_hacks = await localforage.getItem("epube.enable-column-hacks");

			if (column_hacks && Reader.Page._moved_next >= 20) {
				console.log('forcing re-render because of column hacks');
				rendition.onResized($("#reader").width());
				Reader.Page._moved_next = 0;
			}

			const currentCfi = location.start.cfi;
			const currentPct = parseInt(book.locations.percentageFromCfi(currentCfi) * 100);

			$("#cur_page").text(Math.floor(location.start.location / LOCATION_DIVISOR));
			$("#total_pages").text(Math.floor(book.locations.length() / LOCATION_DIVISOR));
			$("#page_pct").text(parseInt(book.locations.percentageFromCfi(currentCfi)*100) + '%');

			Reader.updateTocBarPosition(book, location);

			const displayed = location.start.displayed;

			if (displayed) {
				$("#chapter_cur_page").text(displayed.page);
				$("#chapter_total_pages").text(displayed.total);

				if (displayed.total > 0)
					$("#chapter_pct").text(parseInt(displayed.page / displayed.total * 100) + '%')
			}

			if (Reader.startup_finished && Reader.last_stored_cfi != currentCfi && !Reader.Page._prevent_lastread) {
				const lastread_timestamp = new Date().getTime();

				console.log("storing lastread", currentPct, currentCfi, lastread_timestamp);

				await localforage.setItem(Reader.cacheId("lastread"),
					{cfi: currentCfi, page: currentPct, total: 100, timestamp: lastread_timestamp});

				if (App.isOnline()) {
					console.log("updating remote lastread", currentCfi);
					Reader.last_stored_cfi = currentCfi;

					$.post("backend.php", { op: "storelastread", id: $.urlParam("id"), page: currentPct,
						cfi: currentCfi, timestamp: lastread_timestamp });
				}
			}
		},
		clearLastRead: async function() {
			if (confirm("Clear stored last read location?")) {
				const total = window.book.locations.length();
				const lastread_timestamp = new Date().getTime();

				if (App.isOnline()) {
					const data = await $.post("backend.php", { op: "storelastread", page: PAGE_RESET_PROGRESS, cfi: "", id: $.urlParam("id"), timestamp: lastread_timestamp });

					$(".lastread_input").val(data.page + '%');
				}

				await localforage.setItem(Reader.cacheId("lastread"),
					{cfi: "", page: 0, total: total, timestamp: lastread_timestamp});

				await window.book.rendition.display(window.book.locations.cfiFromPercentage(0));
			}
		},
		openLastRead: async function() {
			$(".loading-message").html("Opening last read page…");

			const rendition = window.book.rendition;

			if (!rendition) {
				console.error('no rendition to work on');
				return;
			}

			let location_found = false;

			const lr_local = await localforage.getItem(Reader.cacheId("lastread")) || {};

			console.log('got local lastread', lr_local);

			if (lr_local && lr_local.cfi) {
				console.log('using local lastread cfi', lr_local.cfi);
				try {
					await Reader.Page.moveToCfiVerify(rendition, lr_local.cfi);
					location_found = true;
				} catch (e) {
					console.warn(e);
				}
			}

			if (App.isOnline()) {
				try {
					const lr_remote = await $.post("backend.php", { op: "getlastread", id: $.urlParam("id") });

					console.log('got remote lastread', lr_remote);

					if (lr_remote) {
						if (lr_remote.cfi && lr_local.cfi != lr_remote.cfi && (lr_remote.timestamp >= (lr_local.timestamp || 0))) {
							console.log(`using remote lastread cfi ${lr_remote.cfi} (timestamp is newer or local timestamp is missing)`);

							await localforage.setItem(Reader.cacheId("lastread"),
								{cfi: lr_remote.cfi, page: lr_remote.page, total: lr_remote.total, timestamp: lr_remote.timestamp});

							await Reader.Page.moveToCfiVerify(rendition, lr_remote.cfi);

							location_found = true;
						}
					}
				} catch (e) {
					console.log('unable to get remote lastread, continuing...');
					console.warn(e);
				}
			}

			// fallback if there's no local nor remote location, open first page
			if (!location_found)
				rendition.display();

			$(".loading").hide();
		},
		moveToCfiVerify: async function(rendition, cfi, attempts = 3) {
			for (let att = 1; att < attempts; att++) {
				Reader.Page.blockLastReadUpdate();

				console.log(`trying to open ${cfi}: attempt ${att} of ${attempts}...`);

				/* eslint-disable no-await-in-loop */
				await rendition.display(cfi); // we need to be synchronous here

				const moved_to = rendition.currentLocation().start.cfi;

				console.log(`opened cfi ${moved_to} expected ${cfi}`);

				if (moved_to == cfi)
					return;
			}
		},
	},
	Settings: {
		onThemeChanged: async function(elem) {
			const theme = $(elem).val();

			await localforage.setItem("epube.theme", theme);
			Reader.applyTheme();
		},
		onLineHeightChanged: async function(elem) {
			const height = $(elem).val();

			await localforage.setItem("epube.lineHeight", height);
			Reader.applyStyles();
		},
		onTextSizeChanged: async function(elem) {
			const size = $(elem).val();

			await localforage.setItem("epube.fontSize", size);
			Reader.applyStyles();
		},
		onFontChanged: async function(elem) {
			const font = $(elem).val();

			await localforage.setItem("epube.fontFamily", font);
			Reader.applyStyles();
		}
	}
};

/* exported __get_reader */
function __get_reader() {
	return Reader;
}

/* exported __get_app */
function __get_app() {
	return App;
}
