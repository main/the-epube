create table if not exists epube_users (
	id serial not null primary key,
	username varchar(100) not null unique,
	pass varchar(200) not null);

create table if not exists epube_pagination (
	id serial not null primary key,
	bookid bigint not null,
	total_pages bigint not null,
	pagination text not null);

create table if not exists epube_books (
	id serial not null primary key,
	bookid bigint not null,
	owner varchar(200) not null not null references epube_users(username) on delete cascade,
	lastts bigint not null,
	lastcfi text not null,
	lastread bigint not null);

create table if not exists epube_favorites(
	id serial not null primary key,
	bookid bigint not null,
	owner varchar(200) not null references epube_users(username) on delete cascade);

create table if not exists epube_sessions (id varchar(250) not null primary key,
	data text,
	expire integer not null);

create index epube_sessions_expire_index on epube_sessions(expire);
