create table if not exists epube_sessions (id varchar(250) not null primary key,
	data text,
	expire integer not null);

create index epube_sessions_expire_index on epube_sessions(expire);
