<?php
	require_once "common.php";

	use Jumbojett\OpenIDConnectClient;

	$session_name = Config::get(Config::SESSION_NAME);
	$session_expire = Config::get(Config::SESSION_LIFETIME);

	if (Config::is_server_https())
		ini_set("session.cookie_secure", "true");

	ini_set("session.name", "epube_sid");
	ini_set("session.use_only_cookies", "true");
	ini_set("session.gc_maxlifetime", $session_expire);
	ini_set("session.cookie_lifetime", "0");

	session_set_cookie_params($session_expire);

	session_save_path(dirname(__DIR__) . "/sessions");

	// prolong PHP session cookie
	if (isset($_COOKIE[$session_name]))
	setcookie($session_name,
		$_COOKIE[$session_name],
		time() + $session_expire,
		ini_get("session.cookie_path"),
		ini_get("session.cookie_domain"),
		ini_get("session.cookie_secure"),
		ini_get("session.cookie_httponly"));

	function validate_session() : bool {
		if (!empty($_SESSION["owner"])) {

			$access_token = $_SESSION["access_token"] ?? null;

			// verify oidc access token once an hour
			if ($access_token && $_SESSION["access_token_last_check"] < time() - 3600) {

					$oidc = new OpenIDConnectClient(Config::get(Config::OIDC_URL),
							Config::get(Config::OIDC_CLIENT_ID),
							Config::get(Config::OIDC_CLIENT_SECRET));

					try {
						$result = $oidc->introspectToken($access_token);

						if (!$result->active)
							return false;

						$_SESSION["access_token_last_check"] = time();

					} catch (Exception $e) {
						return false;
					}
			}

			$user = ORM::for_table('epube_users')
				->where('username', $_SESSION['owner'])
				->find_one();

			if ($user && sha1($user->pass) == $_SESSION['pass_hash']) {
				return true;
			}
		}

		return false;
	}

	function logout_user() : void {
		if (session_status() == PHP_SESSION_ACTIVE) {
			session_destroy();

			if (isset($_COOKIE[session_name()])) {
				setcookie(session_name(), '', time()-42000, '/');
			}

			if (isset($_COOKIE["epube_csrf_token"])) {
				setcookie("epube_csrf_token", '', time()-42000, '/');
			}

			session_commit();
		}
	}

	register_shutdown_function('session_write_close');

	if (Config::get(Config::DB_TYPE) == 'pgsql') {

		function epube_open(string $savePath, string $sessionName): bool {
			return true;
		}

		function epube_read(string $id): string {
			global $session_expire;

			$sth = \Db::pdo()->prepare("SELECT data FROM epube_sessions WHERE id=?");
			$sth->execute([$id]);

			if ($row = $sth->fetch()) {
					return base64_decode($row["data"]);

			} else {
					$expire = time() + $session_expire;

					$sth = \Db::pdo()->prepare("INSERT INTO epube_sessions (id, data, expire)
						VALUES (?, '', ?)");
					$sth->execute([$id, $expire]);

					return "";

			}

		}

		function epube_write(string $id, string $data): bool {
			global $session_expire;

			$data = base64_encode($data);
			$expire = time() + $session_expire;

			$sth = \Db::pdo()->prepare("SELECT id FROM epube_sessions WHERE id=?");
			$sth->execute([$id]);

			if ($sth->fetch()) {
				$sth = \Db::pdo()->prepare("UPDATE epube_sessions SET data=?, expire=? WHERE id=?");
				$sth->execute([$data, $expire, $id]);
			} else {
				$sth = \Db::pdo()->prepare("INSERT INTO epube_sessions (id, data, expire)
					VALUES (?, ?, ?)");
				$sth->execute([$id, $data, $expire]);
			}

			return true;
		}

		function epube_close(): bool {
			return true;
		}

		function epube_destroy(string $id): bool {
			$sth = \Db::pdo()->prepare("DELETE FROM epube_sessions WHERE id = ?");
			$sth->execute([$id]);

			return true;
		}

		function epube_gc(int $lifetime): bool {
			\Db::pdo()->query("DELETE FROM epube_sessions WHERE expire < " . time());

			return true;
		}

		session_set_save_handler('epube_open',
			'epube_close', 'epube_read',
			'epube_write', 'epube_destroy',
			'epube_gc'); // @phpstan-ignore-line
	}

	if (isset($_COOKIE[session_name()])) {
		if (session_status() != PHP_SESSION_ACTIVE)
			session_start();
	}
