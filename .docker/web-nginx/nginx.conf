worker_processes auto;
pid /tmp/nginx.pid;

events {
    worker_connections  1024;
}

http {
	include /etc/nginx/mime.types;
	default_type  application/octet-stream;

	access_log /dev/stdout;
	error_log /dev/stderr warn;

	sendfile on;
	client_max_body_size 64M;

	index index.php;

	resolver ${RESOLVER} valid=5s;

	server {
		listen 8080;
		listen [::]:8080;
		root ${APP_WEB_ROOT};

		location ${APP_BASE}/db {
			internal;
		}

		location ${APP_BASE}/sessions {
			internal;
		}

		location ~ \.php$ {
			# regex to split $uri to $fastcgi_script_name and $fastcgi_path
			fastcgi_split_path_info ^(.+?\.php)(/.*)$;

			# Check that the PHP script exists before passing it
			try_files $fastcgi_script_name =404;

			# Bypass the fact that try_files resets $fastcgi_path_info
			# see: http://trac.nginx.org/nginx/ticket/321
			set $path_info $fastcgi_path_info;
			fastcgi_param PATH_INFO $path_info;

			fastcgi_index index.php;
			include fastcgi.conf;

			set $backend "${APP_UPSTREAM}:9000";

			fastcgi_pass $backend;
		}

		location / {
			try_files $uri $uri/ =404;
		}

	}
}
