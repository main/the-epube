ARG PROXY_REGISTRY
FROM ${PROXY_REGISTRY}alpine:3.20
EXPOSE 9000/tcp

ENV SCRIPT_ROOT=/opt/epube
ENV SRC_DIR=/src/epube

RUN apk add --no-cache php83 php83-fpm \
	php83-pdo php83-gd php83-mbstring php83-pecl-apcu \
	php83-pgsql php83-pdo_pgsql postgresql-client \
	php83-intl php83-xml php83-session php83-opcache \
	php83-dom php83-fileinfo php83-json \
	php83-sqlite3 php83-pdo_sqlite sqlite \
	php83-zip php83-curl php83-openssl git \
	sudo php83-pecl-xdebug rsync && \
	sed -i -e 's/post_max_size = 8M/post_max_size = 64M/' /etc/php83/php.ini && \
	sed -i -e 's/^listen = 127.0.0.1:9000/listen = 9000/' \
		-e 's/;\(clear_env\) = .*/\1 = no/i' \
		-e 's/^\(user\|group\) = .*/\1 = app/i' \
		-e 's/;\(php_admin_value\[error_log\]\) = .*/\1 = \/tmp\/error.log/' \
		-e 's/;\(php_admin_flag\[log_errors\]\) = .*/\1 = on/' \
			/etc/php83/php-fpm.d/www.conf && \
	mkdir -p /var/www ${SCRIPT_ROOT}/config.d

ARG CI_COMMIT_BRANCH
ENV CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH}

ARG CI_COMMIT_SHORT_SHA
ENV CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA}

ARG CI_COMMIT_TIMESTAMP
ENV CI_COMMIT_TIMESTAMP=${CI_COMMIT_TIMESTAMP}

ARG CI_COMMIT_SHA
ENV CI_COMMIT_SHA=${CI_COMMIT_SHA}

ADD .docker/app/startup.sh ${SCRIPT_ROOT}
ADD .docker/app/index.php ${SCRIPT_ROOT}
ADD .docker/app/config.docker.php ${SCRIPT_ROOT}

RUN chmod 0755 ${SCRIPT_ROOT}/startup.sh

COPY . ${SRC_DIR}

ENV PHP_WORKER_MAX_CHILDREN=5
ENV PHP_WORKER_MEMORY_LIMIT=256M

ENV OWNER_UID=1000
ENV OWNER_GID=1000

ENV EPUBE_ADMIN_USER="admin"
ENV EPUBE_ADMIN_PASS="password"

# EPUBE_XDEBUG_HOST defaults to host IP if unset
ENV EPUBE_XDEBUG_ENABLED=""
ENV EPUBE_XDEBUG_HOST=""
ENV EPUBE_XDEBUG_PORT="9000"

ENV EPUBE_DB_TYPE="sqlite"
ENV EPUBE_DB_HOST="db"
ENV EPUBE_DB_PORT="5432"

ENV EPUBE_SCRATCH_DB="db/scratch.db"
ENV EPUBE_BOOKS_DIR="/books"
ENV EPUBE_CALIBRE_DB="/books/metadata.db"
ENV EPUBE_DICT_SERVER="dict"

CMD ${SCRIPT_ROOT}/startup.sh
