const CACHE_PREFIX = 'epube';
const CACHE_NAME = CACHE_PREFIX + '-v3';

self.addEventListener('activate', function(event) {
	event.waitUntil(
		caches.keys().then(function(keyList) {
			return Promise.all(keyList.map((key) => {
				if (key.indexOf(CACHE_PREFIX) != -1 && key != CACHE_NAME) {
					return caches.delete(key);
				}
				return false;
			}));
		})
    );
});

function send_message(client, msg) {
	return client.postMessage(msg);
}

function send_broadcast(msg) {
	return self.clients.matchAll().then((clients) => {
		clients.forEach((client) => {
			send_message(client, msg);
		})
	})
}

self.addEventListener('message', async (event) => {
	console.log('[worker] got message', event.data);

	if (event.data.msg == 'refresh-cache') {
		console.log("[worker] refreshing cache...");

		await send_broadcast('refresh-started');

		const cache = await caches.open(CACHE_NAME);

		await Promise.all(event.data.urls.map(async (url) => {
			const resp = await fetch(url + "?ts=" + Date.now());
			send_broadcast('refreshed:' + resp.url);
			if (resp.ok) {
				console.log('[worker] refresh complete for', resp.url);
				return cache.put(url, resp);
			} else if (resp.status == 404) {
				console.log('[worker] removing obsolete file', resp.url);
				return cache.delete(url);
			}
		}));

		console.log('[worker] refresh finished');

		await send_broadcast('refresh-finished');
	}
});

this.addEventListener('fetch', (event) => {
	if (event.request.url.match('/assets/'))
		return;

	event.respondWith(caches.match(event.request).then((resp) => {
			if (resp) {
				console.log('[worker] cache hit for', event.request.url);
				return resp;
			} else {
				console.log('[worker] cache miss for', event.request.url);
				return fetch(event.request.clone())
					.then((resp) => {
						if (resp.ok && resp.url.match("backend.php\\?op=(cover|getinfo)")) {
							return caches.open(CACHE_NAME).then((cache) => {
								console.log('[worker] caching response', resp.url);
								cache.put(resp.url, resp.clone());
								return resp;
							});
						} else {
							return resp;
						}
					})
					.catch((err) => {
						console.warn('[worker] fetch request failed', event.request.url, err);

						if (event.request.url[event.request.url.length-1] == "/" || event.request.url.match("index.php|offline.html")) {
							return caches.match("offline.html");
						} else if (event.request.url.match("read.html")) {
							return caches.match("read.html");
						} else {
							console.error('[worker] no fetch fallback for ', event.request.url);
						}
					}
				);
			}
		})
	);
});
